import os
from flask import Flask, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import zipfile
import csv
import io
from flask_sqlalchemy import SQLAlchemy
from time import time_ns
from datetime import datetime

db_path = os.path.abspath(os.getcwd())+"//dbdir//database.db"

UPLOAD_FOLDER = 'uploads'
SAVED_LOGS_FOLDER = 'saved_data'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'zip'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SAVED_LOGS_FOLDER'] = SAVED_LOGS_FOLDER
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/traverse'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_path
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)


class GeoPingData(db.Model):
    driver_id = db.Column(db.String(140))
    ping_id = db.Column(db.Integer, primary_key=True)
    coord_lat = db.Column(db.Float)
    coord_long = db.Column(db.Float)
    speed = db.Column(db.Float)
    acc = db.Column(db.Float)
    pingtime = db.Column(db.DateTime, default=datetime.utcnow)

class EventPingData(db.Model):
    driver_id = db.Column(db.String(140))
    event_id = db.Column(db.Integer, primary_key=True)
    event_category = db.Column(db.String(140))
    coord_lat = db.Column(db.Float)
    coord_long = db.Column(db.Float)
    acc = db.Column(db.Float, nullable=True)
    stationary_time = db.Column(db.Float, nullable=True)
    event_timestamp = db.Column(db.DateTime, default=datetime.utcnow)

'''
check if file is a zip containing a single CSV file. If yes, save the csv, print its contents to stdout.
Return 0 on success, -1 on failure. If failure, print error type to stdout ("FILE WAS NOT ZIP" or "ZIP DOES NOT CONTAIN CSV")
'''
def parse_zipped(file_path):
    print("\n\n\n\n\nFILEPATH IS: {}".format(file_path))
    zfp = zipfile.ZipFile(file_path, mode='r', compression=zipfile.ZIP_STORED)
    csv_filename = zfp.namelist()[0]  # see namelist() for the list of files in the archive
    for filename in zfp.namelist():
        print("Filename is {}".format(filename))
        if filename == "log.txt":
            print("NOW READING LOG.TXT")
            with zfp.open(filename) as csv_f:
                csv_f_as_text = io.TextIOWrapper(csv_f)
                my_reader = csv.reader(csv_f_as_text)
                for row in my_reader:
                    tmprow = [x.strip() for x in row]
                    #driver_id, ping_id, loc_lat, loc_long, speed, acc, pingtime
                    d_id = tmprow[0]
                    time_id = time_ns()
                    loc_lat = tmprow[1]
                    loc_long = tmprow[2]
                    p_speed = tmprow[3]
                    if tmprow[4] == "n/a":
                        p_acc = 0
                    else:
                        p_acc = tmprow[4]
                    ping_timestamp = datetime.strptime(tmprow[5], "%m/%d/%Y %H:%M")

                    geo_ping = GeoPingData(
                        driver_id = d_id,
                        ping_id = time_id,
                        coord_lat = loc_lat,
                        coord_long = loc_long,
                        speed = p_speed,
                        acc = p_acc,
                        pingtime = ping_timestamp
                        )
                    db.session.add(geo_ping)
                    db.session.commit()

        elif filename == "events.txt":
            print("NOW READING EVENTS.TXT")
            with zfp.open(filename) as csv_f:
                csv_f_as_text = io.TextIOWrapper(csv_f)
                my_reader = csv.reader(csv_f_as_text)
                for row in my_reader:
                    tmprow = [x.strip() for x in row]
                    # check event type
                    if tmprow[1] == "Long Stationary Period":
                        d_id = tmprow[0]
                        time_id = time_ns()
                        eventname = tmprow[1]
                        stat_time = tmprow[2]
                        loc_lat = tmprow[3]
                        loc_long = tmprow[4]
                        stat_timestamp = datetime.strptime(tmprow[5], "%m/%d/%Y %H:%M")
                        lsp_event = EventPing(
                            driver_id = d_id,
                            event_id=time_id,
                            event_category=eventname,
                            stationary_time=stat_time,
                            coord_lat = loc_lat,
                            coord_long = loc_long,
                            event_timestamp = stat_timestamp
                             )
                        db.session.add(lsp_event)
                        db.session.commit()
                        print("event_id {}, eventname {}, stationary_time {}, lat {}, long {}, timestamp {}".format(time_id, eventname, stat_time, loc_lat, loc_long, stat_timestamp))
                    if tmprow[0] == "Hard Brake":
                        eventname = tmprow[0]
                        acc = tmprow[1]
                        loc_lat = tmprow[2]
                        loc_long = tmprow[3]
                        stat_timestamp = datetime.strptime(tmprow[4], "%m/%d/%Y %H:%M")
                    print(tmprow)


def allowed_file(filename):
  # this has changed from the original example because the original did not work for me
    return filename[-3:].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET'])
def home():
    return "Hi?"

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file: #  and allowed_file(file.filename)
            print("{} found file".format(file.filename))
            filename = secure_filename(file.filename)
            full_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(full_path)

            # for browser, add 'redirect' function on top of 'url_for'

            parse_zipped(full_path)
            return full_path

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


@app.route('/view_latest_log')
def view_last_log():
     last_log_path = os.path.join(app.config['SAVED_LOGS_FOLDER'], 'out.csv')
     with open(last_log_path, 'r') as logFile:
        data=logFile.read()
        data = data.replace('\n', '&nbsp;')
        print(data)
        return '''<!doctype html>
                <title>Log</title>
                <style type="text/css">body\{margin:40px
auto;max-width:650px;line-height:1.6;font-size:18px;color:#444;padding:0
10px\}h1,h2,h3\{line-height:1.2\}</style>
                <h1>Logs</h1>
                <p>{}<p>
                '''.format(data)



if __name__ == '__main__':
    app.run(debug=True)
