package com.example.gpstest;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class SearchResultArrayAdapter extends ArrayAdapter<SearchResult> {
    private Context context;
    private List<SearchResult> searchResults;

    public SearchResultArrayAdapter(Context context, int resource, List<SearchResult> results){
        super(context, resource, results);
        this.context = context;
        this.searchResults = results;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SearchResult searchResult = searchResults.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.search_result_item, null);

        TextView nameTv = view.findViewById(R.id.search_result_textView);
        nameTv.setText(searchResult.getPlaceName());

        TextView distTv = view.findViewById(R.id.distance_textView);
        distTv.setText(String.format(Locale.US, "%.2f km", searchResult.getDistance()/1000));

        return view;
    }
}
