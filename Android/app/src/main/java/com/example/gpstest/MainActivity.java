package com.example.gpstest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Environment;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

// classes needed to add a marker
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationContract;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;

import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, MapboxMap.OnMapClickListener, SearchFragment.Callbacks {

    private static final String TAG = "MainActivity";

    String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/GPSTest";

    BroadcastReceiver mBroadcastReceiver;
    GeofenceUtils mGeofenceUtils;

    EditText mSearchEditText;
    Button mSearchButton;
    MapView mMapView;
    MapboxMap mMapboxMap;

    Double mCurrentLat;
    Double mCurrentLon;

    private Button mStartNavButton;

    private DirectionsRoute currentRoute;
    private NavigationMapRoute navigationMapRoute;
    private boolean hasRoute = false;

    private ViewGroup mContentView;
    SearchFragment mSearchfragment;
    private Style mMapStyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_api_key));
        setContentView(R.layout.activity_main);

        mSearchEditText = findViewById(R.id.search_editText);

        TextView.OnEditorActionListener returnPressed = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    runSearch();
                }
                return true;
            }
        };

        mSearchEditText.setOnEditorActionListener(returnPressed);

        mSearchButton = findViewById(R.id.search_button);

        mSearchButton.setOnClickListener(v -> runSearch());

        mMapView = findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);

        mContentView = findViewById(R.id.activity_main);

        setFullScreen();

        mGeofenceUtils = new GeofenceUtils(this);

        checkPermissions();

        mStartNavButton = findViewById(R.id.start_button);
        mStartNavButton.setEnabled(false);
        mStartNavButton.setBackgroundResource(android.R.color.darker_gray);
        mStartNavButton.setOnClickListener(v -> {
            boolean simulateRoute = false;
            NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                    .directionsRoute(currentRoute)
                    .shouldSimulateRoute(simulateRoute)
                    .build();
            NavigationLauncher.startNavigation(MainActivity.this, options);
        });
    }

    private void runSearch(){
        if(mSearchEditText.hasFocus()){
            hideSoftKeyBoard();
            mSearchEditText.clearFocus();
        }

        if (navigationMapRoute != null) {
            navigationMapRoute.updateRouteArrowVisibilityTo(false);
            navigationMapRoute.updateRouteVisibilityTo(false);
        }

        if(!isEmpty(mSearchEditText)) {
            MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                    .accessToken(getString(R.string.mapbox_api_key))
                    .query(mSearchEditText.getText().toString())
                    .autocomplete(true)
                    .languages("en")
                    .country("AE")
                    .build();
            mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                @Override
                public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                    List<CarmenFeature> results = response.body().features();
                    ArrayList<SearchResult> searchResults = new ArrayList<>();
                    if (results.size() > 0) {
                        for (int i = 0; i < results.size(); i++) {
                            float[] res = new float[3];
                            Location.distanceBetween(mCurrentLat, mCurrentLon, results.get(i).center().latitude(), results.get(i).center().longitude(), res);
                            searchResults.add(new SearchResult(results.get(i).placeName(), results.get(i).center(), res[0]));
                        }
                        mSearchfragment = new SearchFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("results", searchResults);
                        mSearchfragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.placeholder_frame, mSearchfragment)
                                .commit();
                    } else {
                        Log.i(TAG, "onResponse: No results found");
                        Toast.makeText(MainActivity.this, "No results found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void checkPermissions() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            File dir = new File(mPath);
            Log.i(TAG, "onCreate: "+dir.mkdir());
            Intent intent = new Intent(this, LocationService.class);
            intent.putExtra("Sender", "Main");
            ContextCompat.startForegroundService(this, intent);
            ScheduleUtil.scheduleJob(this);
            mMapView.getMapAsync(this);
            mGeofenceUtils.startGeofence();
        }
    }

    private void setFullScreen(){
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LOW_PROFILE
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                |View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mMapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @SuppressLint("MissingPermission")
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                mMapboxMap.setCameraPosition(new CameraPosition.Builder().zoom(10).build());

                LocationComponent locationComponent = mMapboxMap.getLocationComponent();
                locationComponent.activateLocationComponent(MainActivity.this, style);
                locationComponent.setLocationComponentEnabled(true);
                locationComponent.setCameraMode(CameraMode.TRACKING);
                locationComponent.setRenderMode(RenderMode.NORMAL);
                addDestinationIconSymbolLayer(style);
                mMapboxMap.addOnMapClickListener(MainActivity.this);

            }
        });
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mMapView.onSaveInstanceState(outState);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
        setFullScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
        setFullScreen();
        if(mBroadcastReceiver == null){
            mBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    mCurrentLat = intent.getDoubleExtra("lat", 0);
                    mCurrentLon = intent.getDoubleExtra("lon", 0);
                }
            };
        }
        registerReceiver(mBroadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    File dir = new File(mPath);
                    Log.i(TAG, "onCreate: "+dir.mkdir());
                    Intent intent = new Intent(MainActivity.this, LocationService.class);
                    intent.putExtra("Sender", "Main");
                    ContextCompat.startForegroundService(MainActivity.this, intent);
                    ScheduleUtil.scheduleJob(MainActivity.this);
                    mMapView.getMapAsync(this);
                    mGeofenceUtils.startGeofence();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, LocationService.class);
        stopService(intent);
        if(mBroadcastReceiver != null) unregisterReceiver(mBroadcastReceiver);
        mGeofenceUtils.removeGeofences();
        mMapView.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMapClick(@NonNull LatLng point) {

        Layer layer = mMapboxMap.getStyle().getLayer("destination-symbol-layer-id");

        if(hasRoute){
            hasRoute = false;
            mStartNavButton.setEnabled(false);
            mStartNavButton.setBackgroundResource(android.R.color.darker_gray);
            navigationMapRoute.updateRouteArrowVisibilityTo(false);
            navigationMapRoute.updateRouteVisibilityTo(false);
            mSearchEditText.setText("");
            if(layer != null){
                layer.setProperties(visibility(NONE));
            }
            return true;
        }

        if(mCurrentLat == null || mCurrentLon == null){
            return true;
        }

        if(layer != null){
            layer.setProperties(visibility(VISIBLE));
        }

        Point destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
        Point originPoint = Point.fromLngLat(mCurrentLon, mCurrentLat);

        MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                .accessToken(getString(R.string.mapbox_api_key))
                .query(destinationPoint)
                .languages("en")
                .build();

        mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
            @Override
            public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                List<CarmenFeature> results = response.body().features();
                mSearchEditText.setText(results.get(0).placeName());
            }

            @Override
            public void onFailure(Call<GeocodingResponse> call, Throwable t) {

            }
        });

        GeoJsonSource source = mMapboxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }

        getRoute(originPoint, destinationPoint);
        return true;
    }

    private void getRoute(Point origin, Point destination) {
        NavigationRoute.builder(this)
                .accessToken(getString(R.string.mapbox_api_key))
                .origin(origin)
                .destination(destination)
                .language(Locale.ENGLISH)
                .voiceUnits(DirectionsCriteria.METRIC)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (response.body() == null) {

                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }

                        currentRoute = response.body().routes().get(0);

                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mMapView, mMapboxMap, R.style.NavigationMapRoute);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                        mStartNavButton.setEnabled(true);
                        mStartNavButton.setBackgroundResource(android.R.color.holo_orange_dark);
                        hasRoute = true;
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    private void placeMarkerFromList(Point destinationPoint){
        Layer layer = mMapboxMap.getStyle().getLayer("destination-symbol-layer-id");
        if(layer != null){
            layer.setProperties(visibility(VISIBLE));
        }
        Point originPoint = Point.fromLngLat(mCurrentLon, mCurrentLat);

        GeoJsonSource source = mMapboxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }

        getRoute(originPoint, destinationPoint);
    }

    @Override
    public void onItemSelected(SearchResult searchResult) {
        getSupportFragmentManager().beginTransaction().remove(mSearchfragment).commit();
        mSearchEditText.setText(searchResult.getPlaceName());
        placeMarkerFromList(searchResult.getCoords());
    }

    @Override
    public void onClose() {
        getSupportFragmentManager().beginTransaction().remove(mSearchfragment).commit();
    }
}
