package com.example.gpstest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends ListFragment {

    List<SearchResult> searchResults;
    private Callbacks activity;

    public SearchFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        assert b != null;
        searchResults = b.getParcelableArrayList("results");
        SearchResultArrayAdapter adapter = new SearchResultArrayAdapter(getActivity(), R.layout.search_result_item, searchResults);
        setListAdapter(adapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_fragment, container, false);
        ImageButton closeButton = rootView.findViewById(R.id.close_btn);
        closeButton.setOnClickListener(v -> {
            activity.onClose();
        });
        return rootView;
    }

    public interface Callbacks {
        public void onItemSelected(SearchResult searchResult);
        public void onClose();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        SearchResult searchResult = searchResults.get(position);
        activity.onItemSelected(searchResult);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Callbacks) context;
    }
}
