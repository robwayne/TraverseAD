package com.example.gpstest;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.mapbox.geojson.Point;

public class SearchResult implements Parcelable {
    public static final String PLACE_NAME = "placename";
    public static final String PLACE_LAT = "lat";
    public static final String PLACE_LON = "lon";
    public static final String COORDS = "coords";
    public static final String DIST = "distance";

    private String placeName;
    private Point coords;
    private float distance;

    public Point getCoords() {
        return coords;
    }

    public void setCoords(Point coords) {
        this.coords = coords;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public SearchResult(String placeName, Point coords, float distance){
        this.placeName = placeName;
        this.coords = coords;
        this.distance = distance;
    }

    public SearchResult(Bundle b){
        if(b != null){
            this.placeName = b.getString(PLACE_NAME);
            float lat = b.getFloat(PLACE_LAT);
            float lon = b.getFloat(PLACE_LON);
            this.coords = Point.fromLngLat(lon, lat);
            this.distance = b.getFloat(DIST);
        }
    }

    public SearchResult(Parcel in) {
        Bundle b = in.readBundle(getClass().getClassLoader());
        this.placeName = b.getString(PLACE_NAME);
        float lat = b.getFloat(PLACE_LAT);
        float lon = b.getFloat(PLACE_LON);
        this.coords = Point.fromLngLat(lon, lat);
        this.distance = b.getFloat(DIST);
    }

    public Bundle toBundle(){
        Bundle b = new Bundle();
        b.putString(PLACE_NAME, placeName);
        b.putFloat(PLACE_LON, (float) coords.longitude());
        b.putFloat(PLACE_LAT, (float) coords.latitude());
        b.putFloat(DIST, distance);
        return b;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBundle(this.toBundle());
    }

    public static final Parcelable.Creator<SearchResult> CREATOR
            = new Parcelable.Creator<SearchResult>() {
        public SearchResult createFromParcel(Parcel in) {
            return new SearchResult(in);
        }

        public SearchResult[] newArray(int size) {
            return new SearchResult[size];
        }
    };

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
