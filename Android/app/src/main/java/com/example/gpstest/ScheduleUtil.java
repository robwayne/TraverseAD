package com.example.gpstest;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;

public class ScheduleUtil {
    @TargetApi(Build.VERSION_CODES.M)
    public static void scheduleJob(Context context){
        Log.i("ScheduleUtil", "scheduleJob: scheduling job");
        ComponentName serviceComponent = new ComponentName(context, FileJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(15000);
        builder.setOverrideDeadline(1000*60*30);

        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        if (jobScheduler != null) {
            jobScheduler.schedule(builder.build());
        }

    }

}
