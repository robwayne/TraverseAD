package com.example.gpstest;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class GeofenceUtils {
    private static final String TAG = "GeofenceUtils";
    private final int GEOFENCE_REQ_CODE = 0;
    AppCompatActivity mActivity;


    GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;

    GeofenceUtils(AppCompatActivity activity){
        mActivity = activity;
        mGeofencingClient = LocationServices.getGeofencingClient(mActivity);
    }


    private Geofence createGeofence(LatLng latLng, float radius, String id) {
        return new Geofence.Builder()
                .setCircularRegion(latLng.latitude, latLng.longitude, radius)
                .setRequestId(id)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(10000)
                .build();
    }

    private GeofencingRequest createGeofenceRequest(Geofence geofence) {
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL)
                .addGeofence(geofence)
                .build();
    }

    private PendingIntent createGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) return mGeofencePendingIntent;
        Intent intent = new Intent(mActivity.getApplicationContext(), GeofenceTransitionService.class);
        mGeofencePendingIntent = PendingIntent.getService(mActivity.getApplicationContext(), GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private void addGeofence(GeofencingRequest request) {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mGeofencingClient.addGeofences(request, createGeofencePendingIntent()).addOnSuccessListener(mActivity, new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.i(TAG, "onSuccess: geofences added");
                }
            }).addOnFailureListener(mActivity, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    void removeGeofences(){
        List<String> request_ids = Arrays.asList("A5", "A6", "Aldar HQ");
        mGeofencingClient.removeGeofences(request_ids).addOnSuccessListener(mActivity, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i(TAG, "onSuccess: geofences removed");
            }
        });
    }

    private class FencePoint{
        LatLng coords;
        float radius;
        String id;

        FencePoint(LatLng coords, float radius, String id){
            this.coords = coords;
            this.radius = radius;
            this.id = id;
        }
    }

    void startGeofence(){
        ArrayList<FencePoint> fencePoints = new ArrayList<>();
        fencePoints.add(new FencePoint(new LatLng(24.5230114, 54.4344557), 100, "A5"));
        fencePoints.add(new FencePoint(new LatLng(24.522625, 54.435515), 100, "A6"));
        fencePoints.add(new FencePoint(new LatLng(24.441603, 54.575444), 100, "Aldar HQ"));

        for(FencePoint fp : fencePoints){
            Geofence geofence = createGeofence(fp.coords, fp.radius, fp.id);
            GeofencingRequest geofencingRequest = createGeofenceRequest(geofence);
            addGeofence(geofencingRequest);
        }
    }
}
