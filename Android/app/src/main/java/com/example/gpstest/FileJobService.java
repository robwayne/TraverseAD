package com.example.gpstest;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@android.support.annotation.RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class FileJobService extends JobService {
    private static final String TAG = "FileJobService";
    private static final int BUFFER = 2048;
    String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/GPSTest";
    boolean restartWhenDone = false;

    @Override
    public boolean onStartJob(JobParameters params) {
        if(isWifiConnected()){

            Intent intent = new Intent(this, LocationService.class);
            if(stopService(intent)){
                restartWhenDone = true;
            }

            Log.i(TAG, "onStartJob: my test job");

            if(zip()){
                send();
            }else if(restartWhenDone){
                Log.i(TAG, "onStartJob: restarting location service");
                intent.putExtra("Sender", "FileService");
                ContextCompat.startForegroundService(this, intent);
            }

        }
        ScheduleUtil.scheduleJob(getApplicationContext());
        return false;
    }

    private void delete() {
        String fileName = mPath+"/log.txt";
        String zipFileName = mPath+"/log.zip";

        File tf = new File(fileName);
        File zf = new File(zipFileName);
        tf.delete();
        zf.delete();
    }

    private void send() {
        String logFileName = mPath+"/log.zip";

        FileUploadService service = new Retrofit.Builder().baseUrl("https://traverse-server-ad.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(FileUploadService.class);

        File lf = new File(logFileName);
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/zip"), lf);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", lf.getName(), requestFile);

        String descriptionString  = "Compressed Log file";
        RequestBody description = RequestBody.create(MultipartBody.FORM, descriptionString);

        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Log.i(TAG, "onResponse: Successful Upload");
                    delete();
                } else {
                    Log.i(TAG, "onResponse: Response wasn't successful "+response.code());
                }

                if(restartWhenDone){
                    Log.i(TAG, "onStartJob: restarting location service");
                    Intent intent = new Intent(getApplicationContext(), LocationService.class);
                    intent.putExtra("Sender", "FileService");
                    ContextCompat.startForegroundService(getApplicationContext(), intent);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, "onResponse: Something went wrong "+call.request().url().toString());
                if(restartWhenDone){
                    Log.i(TAG, "onStartJob: restarting location service");
                    Intent intent = new Intent(getApplicationContext(), LocationService.class);
                    intent.putExtra("Sender", "FileService");
                    ContextCompat.startForegroundService(getApplicationContext(), intent);
                }
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isWifiConnected() {
        ConnectivityManager cm = getApplicationContext().getSystemService(ConnectivityManager.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && cm != null) {
            NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
            return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
        }
        return false;
    }

    public boolean zip() {
        String[] fileNames = {mPath+"/log.txt", mPath+"/events.txt"};
        String zipFileName = mPath+"/log.zip";
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            byte data[] = new byte[BUFFER];

            for(int i = 0; i < fileNames.length; i++){

                File file = new File(fileNames[i]);
                if(!file.exists()) {
                    continue;
                }

                FileInputStream fi = new FileInputStream(fileNames[i]);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(fileNames[i].substring(fileNames[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
