package com.example.gpstest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.example.gpstest.App.CHANNEL_ID;

public class LocationService extends Service {

    private static final int BUFFER = 2048;
    FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;
    LocationRequest mLocationRequest;
    Location mPreviousLocation;
    double mStationaryTime = 0.0;
    Location mStationaryLocation = null;

    String mPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/GPSTest";
    private boolean mProcessingFile = false;
    private boolean mAlreadyWritten = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent.getStringExtra("Sender").equals("FileService")){
            mStationaryTime = getSharedPreferences("TempPrefs", MODE_PRIVATE).getFloat("mStationaryTime", 0);
            mAlreadyWritten = getSharedPreferences("TempPrefs", MODE_PRIVATE).getBoolean("mAlreadyWritten", false);
        } else {
            mStationaryTime = 0;
            mAlreadyWritten = false;
        }

        Intent notifIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notifIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("GPSTest")
                .setContentText("GPSTest is running in the background")
                .setSmallIcon(R.drawable.ic_android)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(250);
        mLocationRequest.setFastestInterval(250);
        mLocationRequest.setSmallestDisplacement(0);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();
                    String data = "";
                    data += Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)+",";
                    Intent intent = new Intent("location_update");

                    intent.putExtra("coordinates", "lat: " + location.getLatitude() + " lon: " + location.getLongitude());
                    data += location.getLatitude() + "," + location.getLongitude() + ",";
                    float speed = location.getSpeed() * 3.6f;
                    intent.putExtra("speed", "speed: " + speed + "km/h");
                    data += speed+",";
                    intent.putExtra("provider", "provider: " + location.getProvider());
                    intent.putExtra("accuracy", "accuracy: " + location.getAccuracy() + "m");

                    if (mPreviousLocation != null) {
                        double time = (double) (location.getElapsedRealtimeNanos() - mPreviousLocation.getElapsedRealtimeNanos()) / 1000000000;

                        if (time < 2) {
                            float deltaSpeed = location.getSpeed() - mPreviousLocation.getSpeed();
                            float acceleration = (float) (deltaSpeed / time);
                            if(acceleration < -5){
                                String eventData = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)+","+"Hard Brake,"+acceleration+","+location.getLatitude()+","+location.getLongitude()+","+DateFormat.format("MM/dd/yyyy hh:mm", location.getTime()).toString() +"\n";
                                writeToFile("events.txt", eventData);
                            }
                            intent.putExtra("acceleration", "acceleration: " + acceleration + "m/s2");
                            data += acceleration+",";
                        } else {
                            intent.putExtra("acceleration","acceleration: n/a");
                            data += "n/a,";
                        }

                        if (location.getSpeed() == 0 && mPreviousLocation.getSpeed() != 0) {
                            mStationaryLocation = location;
                        } else if (location.getSpeed() == 0 && mPreviousLocation.getSpeed() == 0) {
                            if (mStationaryLocation == null){
                                mStationaryLocation = location;
                            }
                            mStationaryTime += time;

                        } else if (location.getSpeed() != 0 && mPreviousLocation.getSpeed() == 0) {
                            if(mStationaryTime >= 30){
                                if(!mAlreadyWritten){
                                    String eventData = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)+","+"Long Stationary Period,"+mStationaryTime+","+mStationaryLocation.getLatitude()+","+mStationaryLocation.getLongitude()+","+DateFormat.format("MM/dd/yyyy hh:mm", location.getTime()).toString() +"\n";
                                    writeToFile("events.txt", eventData);
                                    mAlreadyWritten = true;
                                    Log.i("LocationService", "onLocationResult: wrote s loc to file");
                                }
                            }
                            mStationaryTime = 0;
                            mStationaryLocation = null;
                            mAlreadyWritten = false;
                        }

                        if (mStationaryLocation != null) {
                            intent.putExtra("s_coordinates", "s lat: " + mStationaryLocation.getLatitude() + " s lon: " + mStationaryLocation.getLongitude());
                        } else {
                            intent.putExtra("s_coordinates","s lat: s lon:");
                        }
                        intent.putExtra("s_time","stationary time: " + mStationaryTime + "s");
                        data += DateFormat.format("MM/dd/yyyy hh:mm", location.getTime()).toString() +"\n";
                    } else {
                        intent.putExtra("s_coordinates","s lat: s lon:");
                        intent.putExtra("s_time","stationary time: " + mStationaryTime + "s");
                        intent.putExtra("acceleration","acceleration: n/a");
                        data += "n/a,"+ DateFormat.format("MM/dd/yyyy hh:mm", location.getTime()).toString() +"\n";
                    }
                    mPreviousLocation = location;

                    if(!mProcessingFile){
                        writeToFile("log.txt", data);
                    }

                    intent.putExtra("lat", location.getLatitude());
                    intent.putExtra("lon", location.getLongitude());

                    sendBroadcast(intent);
                }
            }
        };

        mPreviousLocation = null;

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);

    }

    private void writeToFile(String filename, String data) {
        File file = new File(mPath+"/"+filename);
        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(file, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            if (fileOutputStream != null) {
                fileOutputStream.write(data.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        SharedPreferences.Editor editor = getSharedPreferences("TempPrefs", MODE_PRIVATE).edit();
        editor.putFloat("mStationaryTime", (float) mStationaryTime);
        editor.putBoolean("mAlreadyWritten", mAlreadyWritten);
        editor.apply();
    }
}
